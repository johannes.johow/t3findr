
library(googlesheets)
library(magrittr)
library(geoloc)
# taken from https://github.com/jennybc/googlesheets/blob/master/inst/shiny-examples/10_read-write-private-sheet/global.R
## prepare the OAuth token and set up the target sheet:
##  - do this interactively
##  - do this EXACTLY ONCE
# logtest <- gs_new("logtest", ws_title = "logtest",  input =
#                     data.frame(user="http:_127.0.0.1_/_5627_" ,
#                                timestamp=paste(Sys.time()),
#                                geoloc_lon=as.double(8.6745178),
#                                geoloc_lat=as.double(50.5884837),
#                                gametype = "table-tennis"), trim = TRUE, verbose = FALSE)
# logtest %>%
#   gs_read()
#  shiny_token <- gs_auth() # authenticate w/ your desired Google identity here
#  saveRDS(shiny_token, "shiny_app_token.rds")
#  ss <- gs_title("logtest")
#  ss$sheet_key # 1KGEuAJiYn7HOhpHE6dwQYCHqgGX5cBPiaTvg-sgrpJ8

## if you version control your app, don't forget to ignore the token file!
## e.g., put it into .gitignore


# taken from https://deanattali.com/blog/shiny-persistent-data-storage/
# and https://github.com/ColinFay/geoloc 

sheet_key <- "1KGEuAJiYn7HOhpHE6dwQYCHqgGX5cBPiaTvg-sgrpJ8"
ss <- googlesheets::gs_key(sheet_key)

saveData <- function(data, table = "logtest", really = is.numeric(input$geoloc_lat) & is.numeric(input$geoloc_lon)) {
  # Grab the Google Sheet
  sheet <- gs_title(table)
  # Add the data as a new row
  if(really==TRUE){
  gs_add_row(sheet, input = data)
  } else {
    showNotification("Saving would require a valid geoposition.")
  }
}

loadData <- function(table = "logtest") {
  # Grab the Google Sheet
  sheet <- gs_title(table)
  # Read the data
  gs_read_csv(sheet)
}

ui <-  fluidPage(
  
  titlePanel("GameSetMatch", windowTitle = "GameSetMatch"),
  HTML( 
    markdown::markdownToHTML(text = "When [geoloc (by Colin Fay)](https://github.com/ColinFay/geoloc) meets [googlesheets (by Jenny Bryan)](https://github.com/jennybc/googlesheets) in a [shinyapp](https://shiny.rstudio.com/) for playing outdoor table-tennis, chess, or backgammon.",
                             fragment.only=TRUE)),
    h2("Data Handling and Privacy Policy"),
    tags$p("Your position is required in order to use this web service. No data will be saved until you hit the 'save' button, which will appear in case of consent. Although in case you so save a position of a tabletennis table or a game, provided data does not allow identification of individuals."),
    tags$strong("However, you should be aware that any submitted information you save will be public!"), br(), br(),
    geoloc::button_geoloc("id", "Accept and Proceed"),
  br(),   br(), 
  # Sidebar layout with a input and output definitions ----
  sidebarLayout(
    # Sidebar panel for inputs ----
    sidebarPanel( 
      h3("Step 1"),
      radioButtons("gametype", "Choose your game.", inline=TRUE,
                   choiceNames = list(
                     icon("table-tennis", "fa-2x"), 
                     icon("chess", "fa-2x") ,
                     icon("basketball-ball", "fa-2x") ,
                     icon("dice", "fa-2x") 
                   ),
                   choiceValues = list(
                     "table-tennis", "chess", "basketball-ball", "dice")),
      h3("Step 2"),
      sliderInput("min_available", "How many minutes will you stay here? ",
                  min = 15, max = 240, value = 30,  step = 5),
      h3("Step 3"),
      radioButtons("needs_equip",  label= "Is any equipment needed? If yes, please comment below.", choiceNames = list("No, everything here.", 
                                                                                                                       "Yes, equipment needed."),
                   choiceValues=list(0,1)),
      h3("Step 4"),
      textInput("comment", "Optional comment and submission.", value = "(no comment)"),
      br(),
      conditionalPanel( 
        condition = "input.id>0",  
        actionButton("submit", label="save", icon=icon("cloud-upload-alt"))),
      h3("Map options"),
      radioButtons("hide_tables",  label= "Hide empty tabletennis tables?", choiceNames = list("No, show all.", 
                                                                                               "Yes, hide tables without active game."),
                   choiceValues=list(0,1)),
      conditionalPanel(
        condition = "input.submit>0",  
        h3("URL components"),
        verbatimTextOutput("CurrentTblOut"))),
    mainPanel( 
      tabsetPanel(tabPanel("Map", leaflet::leafletOutput("lf", height = 800)),
                  tabPanel("History (table)",
                           DT::dataTableOutput("LogTblOut", width = 400))))))

server <- function(input, output, session) { 
  
  # Whenever a field is filled, aggregate all form data
  formData <- reactive({
    data <-cbind(user= paste(sep = "_",
                             session$clientData$url_protocol,
                             session$clientData$url_hostname, 
                             session$clientData$url_pathname, 
                             session$clientData$url_port,     
                             session$clientData$url_search
    ), timestamp = paste(Sys.time()), 
    geoloc_lon = paste(input$geoloc_lon), 
    geoloc_lat = paste(input$geoloc_lat), 
    gametype = input$gametype, 
    available_until = paste(Sys.time()+60*input$min_available),
    needs_equip = input$needs_equip,
    comment = input$comment)
    data
  })
  
  # Return the components of the URL in a string:
  # When the Submit button is clicked, save the form data
  observeEvent(input$submit, {
    saveData(formData())
    output$CurrentTblOut <- renderText({
      paste(sep = "",
            "protocol: ", session$clientData$url_protocol, "\n",
            "hostname: ", session$clientData$url_hostname, "\n",
            "pathname: ", session$clientData$url_pathname, "\n",
            "port: ",     session$clientData$url_port,     "\n",
            "search: ",   session$clientData$url_search,   "\n",
            "geoloc_lon: ",   paste(input$geoloc_lon),   "\n",
            "geoloc_lat: ",   paste(input$geoloc_lat),   "\n"
      )
    })
  })  
  # Parse the GET query string
  output$queryText <- renderText({
    query <- parseQueryString(session$clientData$url_search)
    
    # Return a string with key-value pairs
    paste(names(query), query, sep = "=", collapse=", ")
  })
  
  output$LogTblOut <-  DT::renderDataTable({
    input$submit
    loadData()
  })
  
  observeEvent(!is.null(input$id),
  output$lf <- leaflet::renderLeaflet({
    req(input$geoloc_lon)
    req(input$geoloc_lat)
    logtest  <- loadData()
    t3active  <- logtest[as.numeric(as.POSIXct(logtest$available_until))> as.numeric(Sys.time()),]
    t3past   <- logtest[!logtest$user %in% t3active$user,]
    # output$n_games  <- renderText(ifelse(nrow(t3active)==0, "No open games available but you can add a challenge by following the instructions given below.",
    #                                      ifelse(nrow(t3active)==1, "There is still an open game to join or simply add another challenge by following the instructions given below.",
    #                                             paste0("There are ", nrow(t3active), " open games. However, you can still add a new challenge by following the instructions given below."))))
    # 
    iconSet <- leaflet::awesomeIconList(
      "table-tennis" = leaflet::makeAwesomeIcon(icon = "table-tennis", library = "fa", iconColor = "black"),
      "chess" = leaflet::makeAwesomeIcon(icon = "chess", library = "fa", iconColor = "black"),
      "basketball-ball" = leaflet::makeAwesomeIcon(icon = "basketball-ball", library = "fa", iconColor = "black"),
      "dice" = leaflet::makeAwesomeIcon(icon = "dice", library = "fa", iconColor = "black")
    )
    
    my_map <- leaflet::leaflet() %>%
      leaflet::addTiles() %>%
      leaflet::setView(input$geoloc_lon, input$geoloc_lat, zoom = 17) %>%
      leaflet::addMarkers(input$geoloc_lon, input$geoloc_lat, label = "You're here.")
    # 
    # check history for each type
    if (input$hide_tables==0 & any(t3past$gametype=="table-tennis")){
      iconSet[["table-tennis"]]$markerColor  <- "lightgray" # c( "lightgreen", "lightred","lightblue", "beige")[[i]]
      my_map <-  my_map  %>%
        leaflet::addMarkers(t3past$geoloc_lon[t3past$gametype=="table-tennis"],
                            t3past$geoloc_lat[t3past$gametype=="table-tennis"],
                            icon=list(iconUrl = "static/img/table-tennis.png", 
                                      iconSize = c(64, 64)),
                            #                            icon=iconSet[unique(logtest$gametype)[[i]]],
                            label = paste("table-tennis",
                                          t3past$user[
                                            t3past$gametype=="table-tennis"], "(game expired)"))
    }
    # 
    # check active for each type
    for (i in 1:length(unique(logtest$gametype))){
      if (any(t3active$gametype==unique(logtest$gametype)[[i]])){
        iconSet[[i]]$markerColor  <- c( "lightgreen", "lightred","lightblue", "beige")[[i]]
        my_map <-  my_map  %>%
          leaflet::addMarkers(t3active$geoloc_lon[t3active$gametype==unique(logtest$gametype)[[i]]],
                              t3active$geoloc_lat[t3active$gametype==unique(logtest$gametype)[[i]]],
                              #       icon=iconSet,
                              icon=list(iconUrl = paste0("static/img/", unique(logtest$gametype)[[i]], ".png"), 
                                        iconSize = c(64, 64)),
                              label = paste(t3active$gametype[t3active$gametype==unique(logtest$gametype)[[i]]],
                                            t3active$user[t3active$gametype==unique(logtest$gametype)[[i]]], "waits until",
                                            as.POSIXct(t3active$available_until[t3active$gametype==unique(logtest$gametype)[[i]]], origin="1970-01-01")
                              ))
      }
    }
    my_map})
  )
}

shinyApp(ui, server)