# t3findr

Find or report public table tennis tables in a [shinyapp](https://shiny.rstudio.com/) combining [geoloc by (Colin Fay)](https://github.com/ColinFay/geoloc) with [googlesheets (by Jenny Bryan)](https://github.com/jennybc/googlesheets). 